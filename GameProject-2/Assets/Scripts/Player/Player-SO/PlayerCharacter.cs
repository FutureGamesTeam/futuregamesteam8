using UnityEngine;

//Scriptable object for playable characters. Get and sets prefab and base stats of the playable unit.
[CreateAssetMenu(menuName = "PlayerCharacter")]
public class PlayerCharacter : ScriptableObject
{
    [SerializeField] private GameObject playerPrefab;
    [SerializeField] private int hitPoints;
    [SerializeField] private int damage;
    [SerializeField] private float mobility;
    [SerializeField] private float attackSpeed;
    [SerializeField] private Material characterMat;

    public GameObject GetPlayerPrefab()
    {
        return playerPrefab;
    }
    
    public int GetHitPoints()
    {
        return hitPoints;
    }
    
    public int GetDamage()
    {
        return damage;
    }

    public float GetMobility()
    {
        return mobility;
    }

    public float GetAttackSpeed()
    {
        return attackSpeed;
    }

    public Material GetCharacterMaterial()
    {
        return characterMat;
    }
}

