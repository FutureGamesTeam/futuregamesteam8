using UnityEngine;

public class PlayerWalkState : PlayerBaseState
{
    public override void EnterState(PlayerFSM AI)
    {
        AI._hasTarget = false;
        AI._hasDestination = true;
        AI._agent.SetDestination(AI._destination);
    }

    public override void Update(PlayerFSM AI)
    {
        if (AI._velocity < 0.5)
        {
            AI._velocity += Time.deltaTime * AI._acceleration;
            AI._agent.speed += Time.deltaTime * (AI._velocity * 0.1f + 1f);
            AI._anim.SetFloat(AI._velocityHash, AI._velocity);
        }
        
        if (AI._agent.destination != AI._destination)
        {
            AI._agent.SetDestination(AI._destination);
        } //Updates the agents destination if a new destination is set
        
        if (AI._agent.remainingDistance < 0.50f)
        {
            AI.TransitionToState(AI.IdleState);
        } //Transitions into idle state if the remaining distance on the calculated agent path is less that 0.05 units

        //If a target has been selected it checks the distance to the target to determine if it should go into chase or attack state
        if (AI._hasDestination || !AI._hasTarget) return;
        
        if (AI.holdingThrowable &&
            Vector3.Distance(AI.transform.position, AI._target.transform.position) < AI.throwingRange
            && Vector3.Distance(AI.transform.position, AI._target.transform.position) > AI.attackRange)
        {
            AI.TransitionToState(AI.ThrowingState);
        }
        else if (Vector3.Distance(AI.transform.position, AI._target.transform.position) >= AI.attackRange)
        {
            AI.TransitionToState(AI.ChaseState);
        }
        else
        {
            AI.TransitionToState(AI.AttackState);
        }
    }
}
