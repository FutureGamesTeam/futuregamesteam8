using UnityEngine;

public class PlayerChaseState : PlayerBaseState
{
    public override void EnterState(PlayerFSM AI)
    {
        AI._hasDestination = false;
        AI._hasTarget = true;
    }

    public override void Update(PlayerFSM AI)
    {
        if (AI._velocity < 0.5)
        {
            AI._velocity += Time.deltaTime * AI._acceleration;
            AI._agent.speed += Time.deltaTime * (AI._velocity * 0.1f + 1f);
            AI._anim.SetFloat(AI._velocityHash, AI._velocity);
        }
        
        if (!AI._target && !AI._hasDestination)
        {
            AI.TransitionToState(AI.IdleState);
        } //Transitions into idle state if there is no destination set and the target died
        
        if (AI._hasDestination && !AI._hasTarget)
        {
            AI.TransitionToState(AI.WalkState);
        } //Transitions into walk state if a destination has been set
        
        if(AI._target == null){return;}
        
        if (AI._target)
        {
            AI._agent.SetDestination(AI._target.transform.position);
        } //Sets the destination to the targets location

        if (Vector3.Distance(AI.transform.position, AI._target.transform.position) <= AI.attackRange)
        {
            AI.TransitionToState(AI.AttackState);
        } //Transitions into attack state once the target is within attack range
        
    }
}
