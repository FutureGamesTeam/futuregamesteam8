using UnityEngine;

//This script sets the animator to isDead and changes the outline color to black.
public class PlayerDeadState : PlayerBaseState
{
    public override void EnterState(PlayerFSM AI)
    {
        AI._anim.SetTrigger("isDead");
        AI._hasTarget = false;
        AI._hasDestination = false;
        AI.mat.SetColor("_OutlineColor", Color.black);//Sets outline to black
    }

    public override void Update(PlayerFSM AI)
    {
        
    }
}
