using UnityEngine;

public class PlayerIdleState : PlayerBaseState
{

    public override void EnterState(PlayerFSM AI)
    {
        AI._hasTarget = AI._hasDestination = false;
        AI._agent.SetDestination(AI.transform.position);
        AI._velocity = 0;
        AI._anim.SetFloat(AI._velocityHash, AI._velocity);
        AI._agent.speed = AI.mobility;
    }

    public override void Update(PlayerFSM AI)
    {
        if (AI._hasDestination && !AI._hasTarget)
        {
            AI.TransitionToState(AI.WalkState);
        } //Transitions into walk state if a destination has been set

        //Checks the distance between the player and the target to determine if it should go into attack or chase state
        if (!AI._hasTarget || AI._hasDestination) return;
       
        if (AI.holdingThrowable &&
            Vector3.Distance(AI.transform.position, AI._target.transform.position) < AI.throwingRange
            && Vector3.Distance(AI.transform.position, AI._target.transform.position) > AI.attackRange)
        {
            AI.TransitionToState(AI.ThrowingState);
        }
        else if (Vector3.Distance(AI.transform.position, AI._target.transform.position) >= AI.attackRange)
        {
            AI.TransitionToState(AI.ChaseState);
        }
        else
        {
            AI.TransitionToState(AI.AttackState);
        }

    }

}
