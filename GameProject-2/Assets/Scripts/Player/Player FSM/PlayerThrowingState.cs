using System.Collections;
using UnityEngine;

public class PlayerThrowingState : PlayerBaseState
{
    public override void EnterState(PlayerFSM AI)
    {
        AI._hasTarget = true;
        AI._agent.speed = 0;
        AI._hasDestination = false;
        AI._anim.SetTrigger("Throw");

        //======Rotates the player towards the target======//
        var targetDir = AI._target.transform.position - AI.transform.position;
        var newDir = Vector3.RotateTowards(AI.transform.forward, targetDir, Time.deltaTime * AI.turnSpeed + 5, 0.0f);
        AI.transform.rotation = Quaternion.LookRotation(newDir);
        //=================================================//
        
        AI.StartCoroutine(Throw(AI));
    }

    public override void Update(PlayerFSM AI)
    {
        
    }

    //Spawns the last throwable object grabbed and enables the ThrowingWeapon script and disables itemstats script. 
    //Disables the collision collider so it goes through friendly players.
    //Disables the pickup collider so the pickup mechanic isn't activated while throwing.
    //Also sets the throwing weapons target to the players target. Sets holdingThrowable to false and goes to idle.
    private IEnumerator Throw(PlayerFSM AI)
    {
        yield return new WaitForSeconds(1.2f);
        var throwingWeapon = ObjectPool.Instance.SpawnFromPool(AI.throwingWepNumber, new Vector3(AI.transform.position.x,
            AI.transform.position.y + 2, AI.transform.position.z), Quaternion.identity);
        
        throwingWeapon.GetComponent<ThrowingWeapon>().enabled = true;
        throwingWeapon.GetComponent<ThrowingWeapon>().target = AI._target;
        throwingWeapon.GetComponent<ThrowingWeapon>().pickUpCollider.enabled = false;
        throwingWeapon.GetComponent<ItemStats>().enabled = false;
        throwingWeapon.GetComponent<ThrowingWeapon>().collisionCollider.enabled = false;

        AI.holdingThrowable = false;
        
        AI.TransitionToState(AI.IdleState);
    }
}
