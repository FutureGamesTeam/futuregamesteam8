using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class PlayerFSM : MonoBehaviour
{
 #region Variables

    [SerializeField] internal float attackRange = 3f, attackSpeed = 1f, turnSpeed = 4f, throwingRange = 15f;
    
    public bool rescued = false;
    public int healthPoints = 0;
    public int maxHP;
    public int damage = 0;
    public float mobility = 2;

    public bool holdingThrowable = false;

    public int throwingWepNumber;
    //Player material. Will save outside the game for no reason. Dont mind it.
    public Material mat;
    
    internal NavMeshAgent _agent;
    internal bool _hasTarget, _hasDestination, _hasAttacked;
    internal GameObject _target;
    internal Vector3 _destination;
    //----Animations----//
    internal Animator _anim;
    internal int _velocityHash;
    internal float _acceleration = 0.15f;
    internal float _velocity;
    //-----------------//
    
    //----Weapons------//
    private int _oldWeaponDmg = 0;
    //-----------------//
    
    public bool selected;
    public bool died = false;
    private PlayerBaseState _currentState;
    [SerializeField]private HealthBarManager healthBarManager;
    [SerializeField]private Collider rescueTrigger;
    [SerializeField]private ParticleSystem healEffect;
    [SerializeField]private DialogueTrigger dialogueTrigger;
    [SerializeField] private GameObject rescueIcon;

    [SerializeField] private int unitNumber;

    public readonly PlayerIdleState IdleState = new PlayerIdleState();
    public readonly PlayerWalkState WalkState = new PlayerWalkState();
    public readonly PlayerChaseState ChaseState = new PlayerChaseState();
    public readonly PlayerAttackState AttackState = new PlayerAttackState();
    public readonly PlayerThrowingState ThrowingState = new PlayerThrowingState();
    public readonly PlayerDeadState DeadState = new PlayerDeadState();
    
 #endregion

 #region Events
    
    private void OnEnable()
    {
        EventHandler.Current.OnMoveClick += OnMoveClick;
        EventHandler.Current.OnAttackGO += OnAttackGO;
        EventHandler.Current.OnAdReward += AdRewardHeal;
    }

    private void OnDisable()
    {
        EventHandler.Current.OnMoveClick -= OnMoveClick;
        EventHandler.Current.OnAttackGO -= OnAttackGO;
        EventHandler.Current.OnAdReward -= AdRewardHeal;
    }
    
 #endregion
    
    private void Start()
    {
        dialogueTrigger = GetComponent<DialogueTrigger>();
        healEffect = GetComponentInChildren<ParticleSystem>();
        maxHP = healthPoints;
        mat.SetColor("_OutlineColor", Color.black); //Sets outline to black
        _anim = GetComponent<Animator>();
        _velocityHash = Animator.StringToHash("Velocity");
        _agent = GetComponent<NavMeshAgent>();
        _agent.speed = mobility;
        TransitionToState(IdleState);
        if (rescued)
        {
            rescueTrigger.enabled = false;
            rescueIcon.SetActive(false);
        }
        else
        {
            _anim.SetBool("isPrisoner" ,true);
        }
    }

    private void Update()
    {
        _currentState.Update(this);
        
    } //Runs the update function in the currently active state 

    public void TransitionToState(PlayerBaseState state)
    {
        _currentState = state;
        _currentState.EnterState(this);
    } //Changes the current state to what we want to transition into and runs the enter state function once

    //Called from the input manager and highlights the character if selected.
    public void Selected(bool selected)
    {
        if (!rescued) return;

        if (this.selected)
        {
            mat.SetColor("_OutlineColor", Color.black);//Sets outline to black
            this.selected = false;
        }
        else
        {
            mat.SetColor("_OutlineColor", Color.cyan);//Sets outline to cyan
            this.selected = selected;
        }
    } //Bool flip to select and unselect the player to know if it should listen to inputs or not

    private void OnMoveClick(Vector3 v)
    {
        if (!selected) return; //If the player isn't selected don't do anything
        _hasDestination = true;
        _hasTarget = false;
        _destination = v;
        if (_target)
        {
            _target.GetComponent<Enemy_FSM>().lockOnGUI.SetActive(false);
        } //Checks if the target variable has a valid game object assigned, if so it disables the lock on UI
    }

    private void OnAttackGO(GameObject go)
    {
        if (!selected) return; //If the player isn't selected don't do anything
        _hasTarget = true;
        _hasDestination = false;
        if (_target
        ) //If the target variable holds a valid game object (if its not dead already), disable the lock on UI
        {
            _target.GetComponent<Enemy_FSM>().lockOnGUI.SetActive(false);
        }

        //Enables the lock on ui on new target and sets the target variable to the new target
        go.GetComponent<Enemy_FSM>().lockOnGUI.SetActive(true);
        _target = go;
    }

    //How long the player waits til its able to attack again. Also plays an animation.
    internal IEnumerator AttackCooldown()
    {
        _hasAttacked = true;
        _velocity = 0;
        _anim.SetFloat(_velocityHash, _velocity);
        _anim.SetTrigger("Attack");
        yield return new WaitForSeconds(attackSpeed);
        _hasAttacked = false;
    }
    
    //Called from enemy upon getting attacked. If the characters hp is lower then 0 it dies and transition to deadstate.
    public void TakeDamage(int dmg)
    {
        healthPoints -= dmg;
        
        //Math for determining the % health to send to the health bar
        float f = healthPoints;
        f /= maxHP;
        f *= 100;
        healthBarManager.UpdateHealthBar(f);

        if (healthPoints >= 1 || died) return;
        
        died = true;
        TransitionToState(DeadState);
        EventHandler.Current.PlayerDeath(gameObject);
    }

    //If the player is a prisoner upon another character reaching the prisoner rescued is set to true and the 
    //Character can be controlled.
    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player") || rescued) return;
        
        rescued = true;
        EventHandler.Current.PlayerRecruit();
        gameObject.layer = 8;
        rescueIcon.SetActive(false);
        _anim.SetBool("isPrisoner" ,false);
        dialogueTrigger.TriggerDialogue();
        rescueTrigger.enabled = false;
    }

    private void AdRewardHeal()
    {
        HealPlayer(500);
        if (!rescued)  return;
        gameObject.layer = 8;
        died = false;
        _anim.SetTrigger("isAlive");
        TransitionToState(IdleState);
    }
    //Function that is called upon picking up an item.
    public void HealPlayer(int heal)
    {
        //If the heal is bigger then 0 play the heal effect and heal the player
        if (heal > 0)
        {
            healEffect.Play();
            healthPoints += heal;
        }

        if (healthPoints > maxHP)
        {
            healthPoints = maxHP;
        }
        
        float f = healthPoints;
        f /= maxHP;
        f *= 100;
        healthBarManager.UpdateHealthBar(f);
    }

    //Upon switching weapon. Replaces old bonus damage gained from a weapon to the new weapons stats.
    public void OnWeaponEquip(int weaponDmg)
    {
        damage -= _oldWeaponDmg;
        damage += weaponDmg;

        _oldWeaponDmg = weaponDmg;
    }
}
