public abstract class PlayerBaseState
{
    public abstract void EnterState(PlayerFSM AI); //Runs once every time the specific state has been entered
    public abstract void Update(PlayerFSM AI); //Works as a update function for each state
}
