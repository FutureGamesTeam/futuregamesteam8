using UnityEngine;

public class PlayerAttackState : PlayerBaseState
{
    public override void EnterState(PlayerFSM AI)
    {
        AI._hasDestination = false;
        AI._hasTarget = true;
        AI._agent.SetDestination(AI.transform.position);
        AI._agent.speed = AI.mobility;
    }

    public override void Update(PlayerFSM AI)
    {
        if (!AI._target)
        {
            AI.TransitionToState(AI.IdleState);
        } //Transitions into idle state if the target died
        
        if(AI._target == null){return;}
        
        if (Vector3.Distance(AI.transform.position, AI._target.transform.position) > AI.attackRange)
        {
            AI.TransitionToState(AI.ChaseState);
        } //Transitions to chase state if the target is outside of the attack range

        if (AI._hasDestination && !AI._hasTarget)
        {
            AI.TransitionToState(AI.WalkState);
        } //Transitions into walk state if a new destination has been set
        
        //======Rotates the player towards the target======//
        var targetDir = AI._target.transform.position - AI.transform.position;
        var newDir = Vector3.RotateTowards(AI.transform.forward, targetDir, Time.deltaTime * AI.turnSpeed, 0.0f);
        AI.transform.rotation = Quaternion.LookRotation(newDir);
        //=================================================//
        
        if (!AI._hasAttacked) //Runs the attack as soon as the attack cooldown is off
        {
            if (AI._target) //Checks if the target is still active
            {
                AI._target.GetComponent<Enemy_FSM>().OnTakeDamage(AI.damage);
                AI.StartCoroutine(AI.AttackCooldown()); 
            }
        }
    }
}
