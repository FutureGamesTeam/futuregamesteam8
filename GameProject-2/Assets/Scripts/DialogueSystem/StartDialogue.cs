using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Starts a dialogue at the beginning of the game.
public class StartDialogue : MonoBehaviour
{
    [SerializeField]private DialogueTrigger dialogueTrigger;
   
    //Bool to make sure the intro isn't played twice somewhere.
    private bool _intro = false;
    
    private void Start()
    {
        dialogueTrigger = GetComponent<DialogueTrigger>();
        if (_intro) return;
        dialogueTrigger.TriggerDialogue();
        _intro = true;
    }
}
