using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Takes in the dialogue script and starts playing the dialogue. Put this on npcs that you want to have a dialogue. 
//Then trigger this function when you want that dialogue to play.
public class DialogueTrigger : MonoBehaviour
{
    public Dialogue dialogue;

    public void TriggerDialogue()
    {
        DialogueManager.Instance.StartDialogue(dialogue);
        Time.timeScale = 0f;
    }
}
