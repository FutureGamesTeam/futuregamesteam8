using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    public Text nameText;
    public Text dialogueText;
    public Image profile;
    public Queue<string> sentences;
    public Animator anim;
    public static DialogueManager Instance;

    [SerializeField] private float timeBetweenLetter = 0.05f;
    
    //Singleton
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }
    
    //Creates a queue
    void Start()
    {
        sentences = new Queue<string>();
    }

    //Starts the dialogue. Animation is played.
    public void StartDialogue(Dialogue dialogue)
    {
        anim.SetBool("isOpen", true);
        
        nameText.text = dialogue.name;
        profile.sprite = dialogue.profile;
        
        sentences.Clear();

        foreach (var sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    //Plays sentences in queue. 
    public void DisplayNextSentence()
    {
        if (sentences.Count == 0)
        {
            EndDialogue();
            return;
        }

        var sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    //Types the character with a set time interval which is defined by timeBetweenLetter.
    private IEnumerator TypeSentence(string sentence)
    {
        dialogueText.text = "";
        foreach (var letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return new WaitForSecondsRealtime(timeBetweenLetter);
        }
    }

    //Returns time scale to 1 and animates the dialogue box out of the screen.
    private void EndDialogue()
    {
        anim.SetBool("isOpen", false);
        Time.timeScale = 1f;
    }
}
