using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Triggers a dialogue in the boss room
public class BossRoomDialogue : MonoBehaviour
{
    [SerializeField]private DialogueTrigger dialogueTrigger;
    private bool _isTriggered = false;
    
    private void Start()
    {
        dialogueTrigger = GetComponent<DialogueTrigger>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player") || _isTriggered) return;
        dialogueTrigger.TriggerDialogue();
        _isTriggered = true;
    }
}
