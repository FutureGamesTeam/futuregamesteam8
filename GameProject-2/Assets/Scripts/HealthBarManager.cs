using UnityEngine;
using UnityEngine.UI;

public class HealthBarManager : MonoBehaviour
{
    [SerializeField] private Slider slider;
    [SerializeField] private Image fillImage;

    private void Start()
    {
        fillImage.color = Color.green;
    }

    private void Update()
    {
        transform.LookAt(Camera.main.transform);
    }

    public void UpdateHealthBar(float f)
    {
        slider.value = f;
        if (f > 50f)
        {
            f -= 50;
            f *= 2;
            fillImage.color = Color.Lerp(Color.yellow, Color.green, f / 100);
        }
        else
        {
            f *= 2;
            fillImage.color = Color.Lerp(Color.red, Color.yellow, f / 100);
        }
        
    }
}
