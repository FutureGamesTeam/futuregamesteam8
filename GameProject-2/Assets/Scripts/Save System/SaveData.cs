[System.Serializable]
public class SaveData
{
    //This is the what the binary formatter reads and writes to create a save file that is not readable for players
    public bool sfxOn, musicOn;

    public SaveData(GlobalObject globalObject)
    {
        sfxOn = globalObject.sfxSound;
        musicOn = globalObject.musicSound;
    }
}
