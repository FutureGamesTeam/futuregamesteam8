using UnityEngine;

public class GlobalObject : MonoBehaviour
{
    //Object that sits in the main menu and does not get destroyed when loading a new scene, used for saving and loading saves
    
    public static GlobalObject Instance;
    
    public bool musicSound, sfxSound;
    
    private void Awake()
    {
        if(Instance == null)
        {
            DontDestroyOnLoad(gameObject); 
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void Save()
    {
        SaveSystem.Save(this);
    }

    public void Load()
    {
        try
        {
            var data = SaveSystem.Load();
            musicSound = data.musicOn;
            sfxSound = data.sfxOn;
        }
        catch //In case there is no save file it loads the default values, which in this case is on
        {
            musicSound = true;
            sfxSound = true;
        }
    }
}
