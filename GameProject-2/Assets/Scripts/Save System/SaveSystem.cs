using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
    private static readonly string Path = Application.persistentDataPath + "/SaveData.riot"; //a persistent path that works on most devices
    public static void Save(GlobalObject save)
    {
        var formatter = new BinaryFormatter(); //binary formatter
        var stream = new FileStream(Path, FileMode.Create); //opens a stream to the path that is set to create a file, if there is one already with the same name it will override it

        var data = new SaveData(save);

        formatter.Serialize(stream, data); //changes the save into binary format and saves the data from SaveData
        stream.Close(); //closes the stream to the file
    }

    public static SaveData Load()
    {
        if(File.Exists(Path))
        {
            var formatter = new BinaryFormatter(); //binary formatter
            var stream = new FileStream(Path, FileMode.Open); //opens a stream to the persistent path to open the file
            var data = formatter.Deserialize(stream) as SaveData; //changes the save back into readable code from binary and applies it to save data
            stream.Close(); //closes stream
            return data;
        }

        Debug.Log("File not found in " + Path);
        return null;
    }
}
