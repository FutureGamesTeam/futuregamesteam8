using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SpawnManager : MonoBehaviour
{
    #region Variables
    
    [SerializeField][Tooltip("Amount of players per floor (MAX 5 currently)")] private int[] numberOfPlayersPerFloor;
    [SerializeField][Tooltip("Amount of enemies per floor (MAX 10 currently)")] private int[] numberOfEnemiesPerFloor;
    [SerializeField] [Tooltip("Amount of power-ups per floor (MAX 5 currently)")] private int[] numberOfPowerUpsPerFloor;
    [SerializeField][Tooltip("Player class types")] private List<PlayerCharacter> playerCharacters;
    [SerializeField][Tooltip("Enemy class types")] private List<EnemyCharacter> enemyCharacters;
    [SerializeField][Tooltip("Item class types")] private List<Items_SO> itemObjects;
    [Tooltip("Floor object for building NavMesh")] public GameObject[] floorObjects;

    private Transform[][] _allEnemySpawnPoints;
    private Transform[][] _allPlayerSpawnPoints;
    private Transform[][] _allPowerUpSpawnPoints;
    private GameObject[] _players;
    private GameObject[] _enemies;
    private GameObject[] _powerUps;

    //For the hardcoded function. Ignore this abomination.
    [SerializeField]private GameObject[] _charactersLevel2;
    [SerializeField]private Transform[] _spawnLevel2;

    public static SpawnManager Instance;
    #endregion

    #region Events

    private void OnEnable()
    {
        EventHandler.Current.OnChangeFloor += OnChangeFloor;
        EventHandler.Current.OnSpawn += StartSpawn;
    }

    private void OnDisable()
    {
        EventHandler.Current.OnChangeFloor -= OnChangeFloor;
        EventHandler.Current.OnSpawn -= StartSpawn;
    }
    #endregion
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        } //simple singleton
        
        
        _allPlayerSpawnPoints = new Transform[floorObjects.Length][];
        _allEnemySpawnPoints = new Transform[floorObjects.Length][];
        _allPowerUpSpawnPoints = new Transform[floorObjects.Length][];
        //creates a "jagged array" of all the spawnpoints depending on how many floors we have
        //"jagged array" is an array of arrays, each entry in the array is an array of other things, in this case transforms

        for (var i = 0; i < _allEnemySpawnPoints.Length; i++)
        {
            _allEnemySpawnPoints[i] = new Transform[numberOfEnemiesPerFloor[i]];
            for (var j = 0; j < numberOfEnemiesPerFloor[i]; j++)
            {
                _allEnemySpawnPoints[i][j] = GameObject.Find($"enemySpawnPoint ({i},{j})").transform;
            }
        } //sets all the enemy spawnpoints into the jagged array storing them per floor

        for (var i = 0; i < _allPlayerSpawnPoints.Length; i++)
        {
            _allPlayerSpawnPoints[i] = new Transform[numberOfPlayersPerFloor[i]];
            for (var j = 0; j < numberOfPlayersPerFloor[i]; j++)
            {
                _allPlayerSpawnPoints[i][j] = GameObject.Find($"playerSpawnPoint ({i},{j})").transform;
            }
        } //sets all the player spawnpoints into the jagged array, storing them per floor

        for (var i = 0; i < _allPowerUpSpawnPoints.Length; i++) 
        {
            _allPowerUpSpawnPoints[i] = new Transform[numberOfPowerUpsPerFloor[i]];
            for (var j = 0; j < numberOfPowerUpsPerFloor[i]; j++)
            {
                _allPowerUpSpawnPoints[i][j] = GameObject.Find($"powerSpawnPoint ({i},{j})").transform;
            }
        }
        
        // 0,0 is floor 1 spawnpoint 1
        // 1,0 is floor 2 spawnpoint 1
        // 0,1 is floor 1 spawnpoint 2 and so on.
    }

    private void StartSpawn()
    {
        SpawnPlayers(1);
        SpawnEnemies(1);
        SpawnPowerUps(1);
    } //spawns all characters at the start of the game

    /*In the inspector the scriptableObject on element0 will spawn on the spawnpoint set at element0.
    This loop will spawn all prefabs on set spawnpoint. 
    Side note: This makes it possible to randomize 
    where the enemy will spawn for more variety*/
    private void SpawnPlayers(int i)
    {
        Array.Resize(ref _players, numberOfPlayersPerFloor[i-1]);
        //resizes the array of players to fit the amount of players per floor. i in this case is the floor number so we have to subtract 1 to get the right array entry
        
        //Spawns all playercharacters on their spawnpoints.
        for(var j = 0; j <  numberOfPlayersPerFloor[i-1]; j++)
        {
            _players[j] = Instantiate(playerCharacters[j].GetPlayerPrefab(), _allPlayerSpawnPoints[i-1][j]);
            _players[j].GetComponent<NavMeshAgent>().Warp(_allPlayerSpawnPoints[i-1][j].position);
            
            _players[j].GetComponent<PlayerFSM>().healthPoints = playerCharacters[j].GetHitPoints();
            _players[j].GetComponent<PlayerFSM>().damage = playerCharacters[j].GetDamage();
            _players[j].GetComponent<PlayerFSM>().mobility = playerCharacters[j].GetMobility();
            _players[j].GetComponent<PlayerFSM>().attackSpeed = playerCharacters[j].GetAttackSpeed();
            _players[j].GetComponent<PlayerFSM>().mat = playerCharacters[j].GetCharacterMaterial();

            //Spawns the maincharacter on the first floor. After this function is done replace the maincharacters 
            //Scriptable object with a character one.
        }
    }

    

    //This functions spawns all enemies at the bottom floor at the start of the game. And set their base stats and 
    //prefab based on which scriptable object they inherit from.
    private void SpawnEnemies(int i)
    {
        Array.Resize(ref _enemies, numberOfEnemiesPerFloor[i-1]);
        for (var j = 0; j < numberOfEnemiesPerFloor[i-1]; j++)
        {
            _enemies[j] = Instantiate(enemyCharacters[j].GetPlayerPrefab(), _allEnemySpawnPoints[i-1][j]);
            
            _enemies[j].GetComponent<Enemy_FSM>().healthPoints = enemyCharacters[j].GetHitPoints();
            _enemies[j].GetComponent<Enemy_FSM>().damage = enemyCharacters[j].GetDamage();
            _enemies[j].GetComponent<Enemy_FSM>().aggroRange = enemyCharacters[j].GetAggroRange();
            _enemies[j].GetComponent<Enemy_FSM>().attackSpeed = enemyCharacters[j].GetAttackSpeed();
            _enemies[j].GetComponent<Enemy_FSM>().mobility = enemyCharacters[j].GetMobility();
            _enemies[j].GetComponent<Enemy_FSM>().mat = enemyCharacters[j].GetCharacterMaterial();
            _enemies[j].GetComponent<NavMeshAgent>().Warp(_allEnemySpawnPoints[i-1][j].position);
        }
        SendPlayersToEnemies();
    }

    private void SpawnPowerUps(int i)
    {
        Array.Resize(ref _powerUps, numberOfPowerUpsPerFloor[i-1]);
        for (var j = 0; j < numberOfPowerUpsPerFloor[i-1]; j++)
        {
            _powerUps[j] = Instantiate(itemObjects[j].GetItemPrefab(), _allPowerUpSpawnPoints[i-1][j]);

            _powerUps[j].GetComponent<ItemStats>().damage = itemObjects[j].GetDamage();
            _powerUps[j].GetComponent<ItemStats>().healing = itemObjects[j].GetHealing();
            _powerUps[j].GetComponent<ItemStats>().weaponName = itemObjects[j].GetWeaponName();
            _powerUps[j].GetComponent<ItemStats>().isThrowable = itemObjects[j].GetIsThrowable();
            _powerUps[j].GetComponent<ItemStats>().objectPoolNumber = itemObjects[j].GetObjectPoolNumber();
        }
        
    }
    
    //What happens when the floor changes
    private void OnChangeFloor(int i)
    {
        DestroyAllEnemies();
        for (var j = 0; j < floorObjects.Length; j++)
        {
            //floorObjects[j].GetComponent<NavMeshSurface>().enabled = j == i - 1;
            if (i - 1 != j) continue;
            //floorObjects[i-1].GetComponent<NavMeshSurface>().BuildNavMesh();
            for (var x = 0; x < _players.Length; x++)
            {
                _players[x].GetComponent<NavMeshAgent>().Warp(_allPlayerSpawnPoints[i - 1][x].position);
            }
            SpawnEnemies(i);
        }

        SpawnLevel2();
    }

    //Destroys all enemies with the tag "Enemy"
    private void DestroyAllEnemies()
    {
        var enemies = GameObject.FindGameObjectsWithTag("Enemy");

        if (enemies == null) return;
        foreach(var enemy in enemies)
            GameObject.Destroy(enemy);
    }
    
    //Goes through the array of players and sends each GameObject to the SendPlayers function
    private void SendPlayersToEnemies()
    {
        foreach (var GO in _players)
        {
            SendPlayers(GO);
        }
    }
    
    //Sends spawned player GameObjects to an event which sends them to the enemy_AI script.
    private void SendPlayers(GameObject player)
    {
        EventHandler.Current.SendingPlayer(player);
    }


    //Disgusting hardcoded function for the final build. Lack of time drove me to do something so cursed.
    private void SpawnLevel2()
    {
        for (int i = 0; i < 2; i++)
        {
            _charactersLevel2[i] = Instantiate(playerCharacters[i + 6].GetPlayerPrefab(), _spawnLevel2[i]);
            _charactersLevel2[i].GetComponent<NavMeshAgent>().Warp(_spawnLevel2[i].position);
            
            _charactersLevel2[i].GetComponent<PlayerFSM>().healthPoints = playerCharacters[i + 6].GetHitPoints();
            _charactersLevel2[i].GetComponent<PlayerFSM>().damage = playerCharacters[i + 6].GetDamage();
            _charactersLevel2[i].GetComponent<PlayerFSM>().mobility = playerCharacters[i + 6].GetMobility();
            _charactersLevel2[i].GetComponent<PlayerFSM>().attackSpeed = playerCharacters[i + 6].GetAttackSpeed();
            _charactersLevel2[i].GetComponent<PlayerFSM>().mat = playerCharacters[i + 6].GetCharacterMaterial();
            
            SendPlayers(_charactersLevel2[i]);
        }
    }
}
