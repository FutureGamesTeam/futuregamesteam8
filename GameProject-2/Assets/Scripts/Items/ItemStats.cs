using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemStats : MonoBehaviour
{
    public int damage = 0;
    public int healing = 0;
    public string weaponName;
    public bool isThrowable;
    public int objectPoolNumber;
    private bool _pickedUp = false;
    private Vector3 _refVelocity;
    private GameObject _player;

    //Pickup trigger
    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Player") || _pickedUp) return;
        _player = other.gameObject;
        _pickedUp = true;
    }

    private void Update()
    {
        if(_player == null) return;
        
        transform.position = Vector3.SmoothDamp(transform.position, _player.transform.position, 
            ref _refVelocity, Time.deltaTime * 50);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (!other.gameObject.CompareTag("Player")) return;
        if (enabled == false) { return;}
        if (other.gameObject == null) return;
        
        other.gameObject.GetComponent<PlayerFSM>().OnWeaponEquip(damage);
        other.gameObject.GetComponent<PlayerFSM>().HealPlayer(healing);
        if (isThrowable)
        {
            other.gameObject.GetComponent<PlayerFSM>().throwingWepNumber = objectPoolNumber;
            other.gameObject.GetComponent<PlayerFSM>().holdingThrowable = isThrowable;
        }

        other.gameObject.GetComponentInChildren<SwapWeapon>().EquipWeapon(weaponName);
        gameObject.SetActive(false);
    }
}
