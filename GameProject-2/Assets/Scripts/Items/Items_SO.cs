using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Item")]
public class Items_SO : ScriptableObject
{
    [SerializeField] private GameObject itemPrefab;
    [SerializeField] private int damage;
    [SerializeField] private int healing;
    [SerializeField] private string weaponName;
    [SerializeField] private bool isThrowable;
    [SerializeField] private int objectPoolNumber;
    
    public GameObject GetItemPrefab()
    {
        return itemPrefab;
    }

    public int GetDamage()
    {
        return damage;
    }

    public int GetHealing()
    {
        return healing;
    }

    public string GetWeaponName()
    {
        return weaponName;
    }

    public bool GetIsThrowable()
    {
        return isThrowable;
    }

    public int GetObjectPoolNumber()
    {
        return objectPoolNumber;
    }
}
