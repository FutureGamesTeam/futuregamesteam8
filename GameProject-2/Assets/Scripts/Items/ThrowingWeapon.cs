using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using Unity.Mathematics;
using UnityEditor;
using UnityEngine;

public class ThrowingWeapon : MonoBehaviour
{
   public GameObject target;
   public Rigidbody rb;

   public float turnSpeed = 1f;
   public float flySpeed = 5f;

   private Transform _localTransform;
   private ItemStats _stats;
   public Collider collisionCollider;
   public Collider pickUpCollider;

   private void Awake()
   {
      enabled = false;
   }

   private void OnEnable()
   {
      rb = GetComponent<Rigidbody>();
      _localTransform = GetComponent<Transform>();
      _stats = GetComponent<ItemStats>();
   }

   //If the object has recieved a target it rotates towards that target and gains velocity.
   private void FixedUpdate()
   {
      if (!target) 
      {
         GetComponent<ItemStats>().enabled = true; 
         return; 
      }
      
      rb.velocity = _localTransform.forward * flySpeed;

      var targetPos = target.transform.position;
      var targetRot = Quaternion.LookRotation(
         new Vector3(targetPos.x, targetPos.y + 2, targetPos.z) - _localTransform.position);
      
      rb.MoveRotation(Quaternion.RotateTowards(_localTransform.rotation, targetRot, turnSpeed));
   }

   //Upon colliding with an enemy it deals damage to that enemy. Upon colliding with a wall it gets disabled.
   private void OnTriggerEnter(Collider other)
   {
      if (other == null || enabled == false){return;}

      if (other.gameObject.CompareTag("Enemy"))
      {
         other.GetComponent<Enemy_FSM>().OnTakeDamage(_stats.damage);
         
         enabled = false;
         gameObject.SetActive(false);
      }
      else if(other.gameObject.layer == 11)
      {
        gameObject.SetActive(false);
      }
   }
}
