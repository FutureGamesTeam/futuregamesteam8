using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public int number;
        public Items_SO item;
        public int size;
    }

    #region Singleton
    
    public static ObjectPool Instance;

    private void Awake()
    {
        Instance = this;
    }
    
    #endregion

    public List<Pool> pools;
    public Dictionary<int, Queue<GameObject>> poolDictionary;

    //Creates a pool of objects that is disabled on start. The pool is created in the unity editor. 
    //Sets what item to spawn thru scriptable objects and what number in the pool the item is. 
    //size sets how many objects of an item you want to spawn as disabled.
    private void Start()
    {
        poolDictionary = new Dictionary<int, Queue<GameObject>>();

        foreach (var pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (var i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.item.GetItemPrefab());

                obj.GetComponent<ItemStats>().damage = pool.item.GetDamage();
                obj.GetComponent<ItemStats>().healing = pool.item.GetHealing();
                obj.GetComponent<ItemStats>().weaponName = pool.item.GetWeaponName();
                obj.GetComponent<ItemStats>().isThrowable = pool.item.GetIsThrowable();
                obj.GetComponent<ItemStats>().objectPoolNumber = pool.item.GetObjectPoolNumber();

                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }
            
            poolDictionary.Add(pool.number, objectPool);
        }
    }

    //Function that is called when another script wanna spawn something from the object pool. Sets rotation and
    //position to to the position and rotations requested on the functions parameters.
    public GameObject SpawnFromPool(int number, Vector3 position, Quaternion rotation)
    {
        if (!poolDictionary.ContainsKey(number))
        {
            Debug.LogWarning("Pool with number " + number + " doesn't exist");
            return null;
        }
        
        
        GameObject objectToSpawn = poolDictionary[number].Dequeue();
        
        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;
        
        poolDictionary[number].Enqueue(objectToSpawn);

        return objectToSpawn;
    }
}
