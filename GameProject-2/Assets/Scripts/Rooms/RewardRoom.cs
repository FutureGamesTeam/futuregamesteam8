using System.Collections.Generic;
using UnityEngine;

public class RewardRoom : MonoBehaviour
{
    public GameObject uiRewardScreen;
    [SerializeField] private List<GameObject> activePlayers; //This have to be serializeField or it gives error to enemyFSM.
    private DialogueTrigger dialogueTrigger;

    private void OnEnable()
    {
        EventHandler.Current.OnSendingPlayers += AddPlayer;
        EventHandler.Current.OnPlayerDeath += OnPlayerDeath;
        dialogueTrigger = GetComponent<DialogueTrigger>();
    }

    private void OnDisable()
    {
        EventHandler.Current.OnSendingPlayers -= AddPlayer;
        EventHandler.Current.OnPlayerDeath -= OnPlayerDeath;
        dialogueTrigger.TriggerDialogue();
        
    }
    
    //Connects to a button and heals all players by 10hp.
    public void HealPlayers()
    {
        foreach (var player in activePlayers)
        {
            player.GetComponent<PlayerFSM>().HealPlayer(10);
        }
        
        uiRewardScreen.SetActive(false);
        EventHandler.Current.ChangeFloor(2);
        gameObject.SetActive(false);
    }

    //Connects to a button and gives all players 5 max hp.
    public void GetArmor()
    {
        foreach (var player in activePlayers)
        {
            player.GetComponent<PlayerFSM>().maxHP += 5;
            player.GetComponent<PlayerFSM>().HealPlayer(5);
        }
        
        uiRewardScreen.SetActive(false);
        EventHandler.Current.ChangeFloor(2);
        gameObject.SetActive(false);
    }

    //Connects to a button and gives all players 2 damage.
    public void GetWeapons()
    {
        foreach (var player in activePlayers)
        {
            player.GetComponent<PlayerFSM>().damage += 2;
        }
        
        uiRewardScreen.SetActive(false);
        EventHandler.Current.ChangeFloor(2);
        gameObject.SetActive(false);
    }
    
    //Adds currently spawned players to a list thru an event.
    private void AddPlayer(GameObject GO)
    {
        activePlayers.Add(GO);
    }
    
    //Removes a player gameobject from the list of activePlayers upon a player dying.
    private void OnPlayerDeath(GameObject player)
    {
        activePlayers.Remove(player);
    }

    //When the player moves inside the reward area activate the UI for rewards.
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            uiRewardScreen.SetActive(true);
        }
    }
}
