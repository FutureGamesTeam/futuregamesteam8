using UnityEngine;

public class HealRoom : MonoBehaviour
{
    [SerializeField] private int healAmount = 5;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.GetComponent<PlayerFSM>().HealPlayer(healAmount);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            healAmount = 0;
        }
    }
}
