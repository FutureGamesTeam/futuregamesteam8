using System;
using UnityEngine;

[DefaultExecutionOrder(-2)]
public class EventHandler : MonoBehaviour
{
    public static EventHandler Current;

    private void Awake()
    {
        Current = this;
    } //Simple singleton instance of this so every other script can reference this

    public event Action<int> OnChangeFloor;

    public void ChangeFloor(int i)
    {
        OnChangeFloor?.Invoke(i);
    }

    public event Action<Vector3> OnMoveClick;

    public void MoveClick(Vector3 v)
    {
        OnMoveClick?.Invoke(v);
    }
    
    public event Action<Vector2> OnDragClick;

    public void DragClick(Vector2 v)
    {
        OnDragClick?.Invoke(v);
    }

    public event Action<Vector2> OnDrag;

    public void Drag(Vector2 v)
    {
        OnDrag?.Invoke(v);
    }

    public event Action<GameObject> OnSendingPlayers;

    public void SendingPlayer(GameObject player)
    {
        OnSendingPlayers?.Invoke(player);
    }
    
    public event Action<float> OnZoom;

    public void Zoom(float f)
    {
        OnZoom?.Invoke(f);
    }

    public event Action OnSpawn;

    public void Spawn()
    {
        OnSpawn?.Invoke();
    }

    public event Action<GameObject> OnPlayerDeath;

    public void PlayerDeath(GameObject player)
    {
        OnPlayerDeath?.Invoke(player);
    }

    public event Action<GameObject> OnAttackGO;

    public void AttackGO(GameObject go)
    {
        OnAttackGO?.Invoke(go);
    }

    public event Action OnPlayerRecruit;
    public void PlayerRecruit()
    {
        OnPlayerRecruit?.Invoke();
    }

    public event Action OnGameOver;

    public void GameOver()
    {
        OnGameOver?.Invoke();
    }

    public event Action OnAdReward;

    public void AdReward()
    {
        OnAdReward?.Invoke();
    }

}


