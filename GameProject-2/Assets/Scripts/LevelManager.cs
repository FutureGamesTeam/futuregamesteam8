using UnityEngine;
using UnityEngine.AI;

[DefaultExecutionOrder(-1)]
public class LevelManager : MonoBehaviour
{
    [Tooltip("Floor Objects group for enabling and disabling floors")]
    [SerializeField] private GameObject[] levels;

    private NavMeshSurface[] _navMeshSurfaces;

    #region Events

    private void OnEnable()
    {
        EventHandler.Current.OnChangeFloor += OnChangeFloor;
    }

    private void OnDisable()
    {
        EventHandler.Current.OnChangeFloor -= OnChangeFloor;
    }
    #endregion
    

    private void Awake()
    {
        _navMeshSurfaces = new NavMeshSurface[levels.Length];
        for (var i = 0; i < levels.Length; i++)
        {
            _navMeshSurfaces[i] = levels[i].GetComponentInChildren<NavMeshSurface>();
            levels[i].SetActive(i < 1); //sets only level one active on start
        }
        _navMeshSurfaces[0].BuildNavMesh();
    }

    private void OnChangeFloor(int i)
    {
        for (var j = 0; j < levels.Length; j++)
        {
            levels[j].SetActive(i>j); //sets current and lower levels active
        }
    }
}
