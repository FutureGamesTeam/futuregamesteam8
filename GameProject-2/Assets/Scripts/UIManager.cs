using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    [SerializeField] private GameObject musicToggleOn, musicToggleOff, sfxToggleOn, sfxToggleOff;
    [SerializeField] private GameObject pauseMenu, resumeCountdown, gameOverMenu, resurrectCountdown;
    [SerializeField] private AudioMixer musicMaster, sfxMaster;

    private TextMeshProUGUI _countdownText;

    private void OnEnable()
    {
        EventHandler.Current.OnGameOver += OnGameOver;
        EventHandler.Current.OnAdReward += OnAdFinished;
    }

    private void OnDisable()
    {
        EventHandler.Current.OnGameOver -= OnGameOver;
        EventHandler.Current.OnAdReward -= OnAdFinished;
    }

    private void Awake()
    {
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
        gameOverMenu.SetActive(false);
        _countdownText = resumeCountdown.GetComponentInChildren<TextMeshProUGUI>();
        try
        {
            GlobalObject.Instance.Load();
            if (GlobalObject.Instance.musicSound)
            {
                musicToggleOff.SetActive(true);
                musicToggleOn.SetActive(false);
                musicMaster.SetFloat("volume", 0f);
            }
            else
            {
                musicToggleOff.SetActive(false);
                musicToggleOn.SetActive(true);
                musicMaster.SetFloat("volume", -80f);
            }

            if (GlobalObject.Instance.sfxSound)
            {
                sfxToggleOff.SetActive(true);
                sfxToggleOn.SetActive(false);
                sfxMaster.SetFloat("volume", 0f);
            }
            else
            {
                sfxToggleOff.SetActive(false);
                sfxToggleOn.SetActive(true);
                sfxMaster.SetFloat("volume", -80f);
            }
        }catch
        {
            Debug.LogError("No GlobalObject found in scene, default settings have been loaded. (Start from main menu scene to bypass this error)");
            musicToggleOff.SetActive(true);
            musicToggleOn.SetActive(false);
            sfxToggleOff.SetActive(true);
            sfxToggleOn.SetActive(false);
            musicMaster.SetFloat("volume", 0f);
            sfxMaster.SetFloat("volume", 0f);
        }
    }

    public void OnPauseButtonClick()
    {
        Time.timeScale = 0f;
        
        pauseMenu.SetActive(true);
        resumeCountdown.SetActive(false);
    }

    public void OnResumeButtonClick()
    {
        StartCoroutine(ResumeCountdown());
    }

    private IEnumerator ResumeCountdown()
    {
        resumeCountdown.SetActive(true);
        _countdownText.text = "3";
        yield return new WaitForSecondsRealtime(1f);
        _countdownText.text = "2";
        yield return new WaitForSecondsRealtime(1f);
        _countdownText.text = "1";
        yield return new WaitForSecondsRealtime(1f);
        pauseMenu.SetActive(false);
        Time.timeScale = 1f;
    }

    public void OnRestartButtonClick()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void OnQuitHomeButtonClick()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    public void OnSFXButtonClick(bool b)
    {
        //true = on
        GlobalObject.Instance.sfxSound = b;
        GlobalObject.Instance.Save();
        sfxMaster.SetFloat("volume", b ? 0f : -80f);
    }

    public void OnMusicButtonClick(bool b)
    {
        //true = on
        GlobalObject.Instance.musicSound = b;
        GlobalObject.Instance.Save();
        musicMaster.SetFloat("volume", b ? 0f : -80f);
    }

    private void OnGameOver()
    {
        Time.timeScale = 0f;
        gameOverMenu.SetActive(true);
        resurrectCountdown.GetComponentInChildren<TextMeshProUGUI>().text = "Resurrect!";
    }

    private void OnAdFinished()
    {
        StartCoroutine(ResumeAfterAd());
    }

    private IEnumerator ResumeAfterAd()
    {
        resurrectCountdown.GetComponentInChildren<TextMeshProUGUI>().text = "3";
        yield return new WaitForSecondsRealtime(1f);
        resurrectCountdown.GetComponentInChildren<TextMeshProUGUI>().text = "2";
        yield return new WaitForSecondsRealtime(1f);
        resurrectCountdown.GetComponentInChildren<TextMeshProUGUI>().text = "1";
        yield return new WaitForSecondsRealtime(1f);
        Time.timeScale = 1f;
        gameOverMenu.SetActive(false);
    }
}
