using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    [SerializeField] private GameObject sfxToggleONButton, sfxToggleOffButton, musicToggleONButton, musicToggleOffButton;

    public void OnStartGameButtonClick()
    {
        SceneManager.LoadScene(1);
    }

    public void OnSettingsButtonClick()
    {
        GlobalObject.Instance.Load();
        if (GlobalObject.Instance.musicSound)
        {
            musicToggleONButton.SetActive(false);
            musicToggleOffButton.SetActive(true);
        }
        else
        {
            musicToggleONButton.SetActive(true);
            musicToggleOffButton.SetActive(false);
        }

        if (GlobalObject.Instance.sfxSound)
        {
            sfxToggleONButton.SetActive(false);
            sfxToggleONButton.SetActive(true);
        }
        else
        {
            sfxToggleONButton.SetActive(true);
            sfxToggleOffButton.SetActive(false);
        }
    }
    
    public void OnSFXButtonClick(bool b)
    {
        //true = on
        GlobalObject.Instance.sfxSound = b;
        GlobalObject.Instance.Save();
    }

    public void OnMusicButtonClick(bool b)
    {
        //true = on
        GlobalObject.Instance.musicSound = b;
        GlobalObject.Instance.Save();
    }

    public void OnQuitGameButtonClick()
    {
        Application.Quit();
    }
}
