﻿using UnityEngine;
using UnityEngine.Advertisements;

public class AdManager : MonoBehaviour, IUnityAdsListener
{
    private readonly string playStoreID = "4127146";
    private readonly string appStoreID = "4127147";

    private readonly string androidInterstitialAd = "Interstitial_Android";
    private readonly string androidRewardedVideoAd = "Rewarded_Android";

    private readonly string iOSInterstitialAd = "Interstitial_iOS";
    private readonly string iOSRewardedAd = "Rewarded_iOS";

    public bool isTargetPlayStore;
    public bool isTestAd;

    private void Start()
    {
        Advertisement.AddListener(this);
        InitializeAdvertisement();
    }

    private void InitializeAdvertisement()
    {
        if (isTargetPlayStore)
        {
            Advertisement.Initialize(playStoreID, isTestAd);
            return;
        }
        Advertisement.Initialize(appStoreID, isTestAd);
    }

    public void PlayInterstitialAd() //If we wanna play unrewarded ads, not currently implemented
    {
        if (isTargetPlayStore)
        {
            if (!Advertisement.IsReady(androidInterstitialAd))
            {
                return;
            }
            Advertisement.Show(androidInterstitialAd);
        }
        else
        {
            if (!Advertisement.IsReady(iOSInterstitialAd))
            {
                return;
            }
            Advertisement.Show(iOSInterstitialAd);
        }
    }

    public void PlayRewardedVideoAd() //Plays an ad that gives a reward if finished
    {
        if (isTargetPlayStore)
        {
            if (!Advertisement.IsReady(androidRewardedVideoAd))
            {
                return;
            }
            Advertisement.Show(androidRewardedVideoAd);
        }
        else
        {
            if (!Advertisement.IsReady(iOSRewardedAd))
            {
                return;
            }
            Advertisement.Show(iOSRewardedAd);
        }
    }

    public void OnUnityAdsReady(string placementId)
    {

    }

    public void OnUnityAdsDidError(string message)
    {
        
    }

    public void OnUnityAdsDidStart(string placementId)
    {

    }

    public void OnUnityAdsDidFinish(string placementId, ShowResult showResult)
    {
        switch (showResult)
        {
            case ShowResult.Failed:
                break;
            case ShowResult.Skipped:
                break;
            case ShowResult.Finished:
                if (placementId == androidRewardedVideoAd || placementId == iOSRewardedAd)
                {
                    EventHandler.Current.AdReward();
                }
                break;
            default:
                break;
        }
    }
}
