using UnityEngine;

public class GameManager : MonoBehaviour
{
    private int _numberOfPlayers = 1;
    private int _numberOfPlayersTotal = 1;
    

    #region Events

    private void OnEnable()
    {
        EventHandler.Current.OnPlayerRecruit += PlayerRecruit;
        EventHandler.Current.OnPlayerDeath += PlayerDeath;
        EventHandler.Current.OnAdReward += AdRewardHeal;
    }

    private void OnDisable()
    {
        EventHandler.Current.OnPlayerRecruit -= PlayerRecruit;
        EventHandler.Current.OnPlayerDeath -= PlayerDeath;
        EventHandler.Current.OnAdReward -= AdRewardHeal;
    }

    #endregion
    

    private void Start()
    {
        EventHandler.Current.Spawn();
    }

    private void Update()
    {
        if (_numberOfPlayers < 1)
        {
            GameOver();
        }
    }

    private void GameOver()
    {
        EventHandler.Current.GameOver();
    }

    private void PlayerRecruit()
    {
        _numberOfPlayers++;
        _numberOfPlayersTotal++;
    }

    private void PlayerDeath(GameObject go)
    {
        _numberOfPlayers--;
    }

    private void AdRewardHeal()
    {
        _numberOfPlayers = _numberOfPlayersTotal;
    }
}
