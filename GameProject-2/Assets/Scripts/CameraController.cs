using UnityEngine;

public class CameraController : MonoBehaviour
{
    #region Variables

    [SerializeField] private float cameraSpeed = 11;
    [SerializeField] private float minZoom = 20, maxZoom = 90;
    [SerializeField] private float maxXPan, maxZPan;
    private Vector3 _newPos, _dragStartPosition, _dragCurrentPosition;

    private Camera _camera;
    
    private Vector3 _newZoom;
    private readonly Vector3 _zoomAmount = new Vector3(0,-10,10);

    [SerializeField] private GameObject[] floors;
    
    #endregion

    #region Events
    private void OnEnable()
    {
        EventHandler.Current.OnDragClick += OnDragClick;
        EventHandler.Current.OnDrag += OnDrag;
        EventHandler.Current.OnChangeFloor += OnChangeFloor;
        EventHandler.Current.OnZoom += OnZoom;
    }

    private void OnDisable()
    {
        EventHandler.Current.OnDragClick -= OnDragClick;
        EventHandler.Current.OnDrag -= OnDrag;
        EventHandler.Current.OnChangeFloor -= OnChangeFloor;
        EventHandler.Current.OnZoom -= OnZoom;
    }
    #endregion

    private void Awake()
    {
        _camera = Camera.main;
        _newZoom = _camera.transform.localPosition;
    } //sets the camera and the current position of the camera in local space to reference for the zoom function

    private void Start()
    {
        _newPos = transform.position;
    } //sets _newPos (which is the desired position the camera should go to) to the camera rigs current position

    private void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, _newPos, Time.deltaTime * cameraSpeed);
    } //updates the position with a Lerp function to smoothen the camera movement between its current position and _newPos

    private void OnDragClick(Vector2 v)
    {
        var plane = new Plane(Vector3.up, Vector3.zero);
        var ray = _camera.ScreenPointToRay(v);
        if (!plane.Raycast(ray, out var entry)) return;
        _dragStartPosition = ray.GetPoint(entry);
    } //creates a plane and sets the start position of the drag

    private void OnDrag(Vector2 v)
    {
        var plane = new Plane(Vector3.up, Vector3.zero);
        var ray = _camera.ScreenPointToRay(v);
        if (!plane.Raycast(ray, out var entry)) return;
        _dragCurrentPosition = ray.GetPoint(entry);
        _newPos = transform.position + _dragStartPosition - _dragCurrentPosition;
        _newPos.x = Mathf.Clamp(_newPos.x, maxXPan * -1, maxXPan);
        _newPos.z = Mathf.Clamp(_newPos.z, maxZPan * -1, maxZPan);
    } //same as OnDragClick but instead updates _newPos depending on where your start and current drag position is

    private void OnChangeFloor(int i)
    {
        _newPos = floors[i-1].transform.position;
    } //sets the _newPos to the center of the new floor that is visible

    private void OnZoom(float f)
    {
        _newZoom += _zoomAmount * f;
        _newZoom.y = Mathf.Clamp(_newZoom.y, minZoom, maxZoom);
        _newZoom.z = Mathf.Clamp(_newZoom.z, maxZoom * -1, minZoom * -1);
        _camera.transform.localPosition = Vector3.Lerp(_camera.transform.localPosition, _newZoom, Time.deltaTime * cameraSpeed);
    } //Lerp between the current position and the desired zoom (runs all the time in Windows via InputManager and only during >1 touch inputs on mobile)
}