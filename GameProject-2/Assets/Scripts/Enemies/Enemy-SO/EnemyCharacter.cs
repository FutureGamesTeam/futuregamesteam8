using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Scriptable object for enemies. Get and sets prefab and base stats of the enemy unit.
[CreateAssetMenu(menuName = "EnemyCharacter")]
public class EnemyCharacter : ScriptableObject
{
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private int hitPoints;
    [SerializeField] private int damage;
    [SerializeField] private float mobility;
    [SerializeField] private float attackSpeed;
    [SerializeField] private float aggroRange;
    [SerializeField] private Material characterMat;

    public GameObject GetPlayerPrefab()
    {
        return enemyPrefab;
    }
    
    public int GetHitPoints()
    {
        return hitPoints;
    }
    
    public int GetDamage()
    {
        return damage;
    }

    public float GetMobility()
    {
        return mobility;
    }

    public float GetAttackSpeed()
    {
        return attackSpeed;
    }

    public float GetAggroRange()
    {
        return aggroRange;
    }
    
    public Material GetCharacterMaterial()
    {
        return characterMat;
    }
}
