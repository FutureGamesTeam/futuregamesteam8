using UnityEngine;

public class ChasingState : AIBaseState
{
    public override void EnterState(Enemy_FSM AI)
    {
        AI._closestDistance = 1000;
    }

    public override void Update(Enemy_FSM AI)
    {
        //Checks all players in the player list and calculates which player is closest. 
        //It then sets the closest player to be its target.
        foreach (var t in AI._players)
        {
            AI._distance = Vector3.Distance(AI.transform.position, t.transform.position);

            if (!(AI._closestDistance > AI._distance)) continue;
            
            AI._closestDistance = AI._distance;
            AI._closestTarget = t;
        }

        //To avoid null references and crashes.
        if (AI._closestTarget == null)
        {
            AI.TransitionToState(AI.ChasingState);
            return;
        }

        //Increases the AI:s speed over time until it reaches its top speed.
        if (AI._velocity < 0.5)
        {
            AI._velocity += Time.deltaTime * AI._acceleration;
            AI.enemyAgent.speed += Time.deltaTime * (AI._velocity * 0.1f + 1f);
            AI.anim.SetFloat(AI._velocityHash, AI._velocity);
        }

        //If the target is within attack range the AI goes to the attackingState
        if (Vector3.Distance(AI.transform.position, AI._closestTarget.transform.position) <= AI.attackRange)
        {
            AI.TransitionToState(AI.AttackingState);
        }
        
        AI.enemyAgent.SetDestination(AI._closestTarget.transform.position);

    }
}
