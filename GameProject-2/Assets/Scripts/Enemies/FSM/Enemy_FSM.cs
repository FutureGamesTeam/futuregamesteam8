using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class Enemy_FSM : MonoBehaviour
{
    #region Variables
    [SerializeField] internal NavMeshAgent enemyAgent;
    [SerializeField] internal List<GameObject> _players;
    [SerializeField] private List<GameObject> _activePlayers;

    //Player material. Will save outside the game for no reason. Dont mind it.
    public Material mat;
    
    internal float _distance;
    internal float _closestDistance = 1000;
    [SerializeField] internal GameObject _closestTarget;
    internal bool attacked = false;
    [SerializeField] internal float attackRange = 3f;
    [SerializeField] public GameObject lockOnGUI;
    [SerializeField] private int flankingCount = 0;
    //-------Animation------//
    [SerializeField] internal Animator anim;
    internal int _velocityHash;
    internal float _velocity = 0.0f;

    internal float _acceleration = 0.15f;
    //---------------------//

    internal Vector3 _patrollingPosition;
    [SerializeField]private HealthBarManager _healthBarManager;
    //Base stats
    public int healthPoints = 0;
    public int damage = 0;
    public float attackSpeed = 0;
    public float aggroRange = 10;
    public float mobility = 0;

    public bool died = false;
    private int _maxHP;
    
    //States
    private AIBaseState _currentState;

    public readonly ChasingState ChasingState = new ChasingState();
    public readonly PatrollingState PatrollingState = new PatrollingState();
    public readonly AttackingState AttackingState = new AttackingState();
    public readonly DeadState DeadState = new DeadState();
    #endregion
    
    #region Events

    private void OnEnable()
    {
        EventHandler.Current.OnSendingPlayers += AddPlayerToList;
        EventHandler.Current.OnPlayerDeath += OnPlayerDeath;
        EventHandler.Current.OnAdReward += OnResurrection;
    }

    private void OnDisable()
    {
        EventHandler.Current.OnSendingPlayers -= AddPlayerToList;
        EventHandler.Current.OnPlayerDeath -= OnPlayerDeath;
        EventHandler.Current.OnAdReward -= OnResurrection;
    }

    #endregion
    
    private void Awake()
    {
        enemyAgent = GetComponent<NavMeshAgent>();
        enemyAgent.stoppingDistance = attackRange;
    }

    private void Start()
    {
        _maxHP = healthPoints;
        anim = GetComponent<Animator>();
        _velocityHash = Animator.StringToHash("Velocity");
        mat.SetColor("_OutlineColor", Color.black); //Sets outline to black
        enemyAgent.speed = mobility;
        _patrollingPosition = transform.position;
        _closestTarget = _players[0];
        TransitionToState(PatrollingState);
    }
    
    private void Update()
    {
        _currentState.Update(this);

        mat.SetColor("_OutlineColor", lockOnGUI.activeSelf ? Color.yellow : Color.black); //If attacked outline yellow if not black.
    }

    public void TransitionToState(AIBaseState state)
    {
        _currentState = state;
        _currentState.EnterState(this);
    }

    //Add all player objects spawned to a list.
    private void AddPlayerToList(GameObject GO)
    {
        _players.Add(GO);
        _activePlayers.Add(GO);
    }

    internal void MoveToTarget(GameObject target)
    {
        enemyAgent.SetDestination(target.transform.position);

        foreach (var player in _players)
        {
            if (Vector3.Distance(transform.position, player.transform.position) < attackRange && attacked == false)
            {
                StartCoroutine(AttackCooldown());
                player.GetComponent<PlayerFSM>().TakeDamage(damage);
            }
        }
    }

    //How many seconds the AI has to wait before calling the attack function again.
    internal IEnumerator AttackCooldown()
    {
        _velocity = 0;
        anim.SetFloat(_velocityHash, _velocity);
        anim.SetTrigger("Attack");
        attacked = true;
        yield return new WaitForSeconds(attackSpeed);
        attacked = false;
    }
    
    //Is called upon getting hit by a player.
    public void OnTakeDamage(int dmg)
    {
        //If flanking count is less then 2 then dont add any extra damage.
        //Else add 10% bonus damage per flanking count.
        if (flankingCount < 2)
        {
            healthPoints -= dmg;
        }
        else
        {
            healthPoints -= Mathf.RoundToInt(dmg * ((flankingCount * 0.1f) + 1));
        }

        if (healthPoints < 1 && !died)
        {
            died = true;
            TransitionToState(DeadState);
            StartCoroutine(Die());
        }
        
        float f = healthPoints;
        f /= _maxHP;
        f *= 100;
        
        _healthBarManager.UpdateHealthBar(f);
    }
    
    //What happens when the object dies
    private IEnumerator Die()
    {
        //Add cool dying effects here
        gameObject.layer = LayerMask.NameToLayer("Floor");
        mat.SetColor("_OutlineColor", Color.black);
        yield return new WaitForSeconds(0.5f);
        var i = Random.Range(0, ObjectPool.Instance.pools.Count);

        ObjectPool.Instance.SpawnFromPool(i, transform.position, quaternion.identity);
    }

    //Adds to flanking count. Meaning the more players around the target the more damage they take.
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            flankingCount++;
        }
    }

    //Removes from flanking count.
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            flankingCount--;
        }
    }
    
    //Receives a player gameobject add removes it from the list
    private void OnPlayerDeath(GameObject player)
    {
        _players.Remove(player);
    }

    //Sets the player list to all player spawned 
    private void OnResurrection()
    {
        _players = _activePlayers;
    }
}
