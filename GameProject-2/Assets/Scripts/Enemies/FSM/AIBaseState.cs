public abstract class AIBaseState
{
    public abstract void EnterState(Enemy_FSM AI);
    
    public abstract void Update(Enemy_FSM AI);
    
}
