using UnityEngine;

public class AttackingState : AIBaseState
{
    public override void EnterState(Enemy_FSM AI)
    {
        AI.enemyAgent.speed = AI.mobility;
    }
    
    public override void Update(Enemy_FSM AI)
    {
        //If attacked bool is false the AI will attack a player.
        //It then waits for the coroutine to set attacked to false again.
        if (AI.attacked == false)
        {
            AI.StartCoroutine(AI.AttackCooldown());
            AI._closestTarget.GetComponent<PlayerFSM>().TakeDamage(AI.damage);
        }

        //If the AI doesn't have a target or the current target dies it returns to patrollingState
        //Else if the target is outside the attack range it changes to chasing state
        if (AI._closestTarget == null || AI._closestTarget.GetComponent<PlayerFSM>().died)
        {
            AI.TransitionToState(AI.PatrollingState);
        }
        else if ((Vector3.Distance(AI.transform.position, AI._closestTarget.transform.position) > AI.attackRange))
        {
            AI.TransitionToState(AI.ChasingState);
        }
    }
}
