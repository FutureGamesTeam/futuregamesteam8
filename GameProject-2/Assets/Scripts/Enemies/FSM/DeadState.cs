public class DeadState : AIBaseState
{
    public override void EnterState(Enemy_FSM AI)
    {
        AI.anim.SetTrigger("isDead");

        AI.enabled = false;
    }

    public override void Update(Enemy_FSM AI)
    {
        
    }
}
