using UnityEngine;

public class PatrollingState : AIBaseState
{
    private bool _inPosition = false;
    public override void EnterState(Enemy_FSM AI)
    {
        AI.enemyAgent.SetDestination(AI._patrollingPosition);
    }

    public override void Update(Enemy_FSM AI)
    {
        //If the agent is standing still set velocity to 0 and change the velocity parameter in the animator
        if (AI.enemyAgent.velocity == Vector3.zero && !_inPosition)
        {
            _inPosition = true;
            AI._velocity = 0;
            AI.anim.SetFloat(AI._velocityHash, AI._velocity);
        }
        
        //If a player from the list players is within aggro range the AI goes to Chasing State.
        foreach (var t in AI._players)
        {
            if (Vector3.Distance(AI.transform.position, t.transform.position) < AI.aggroRange)
            {
                AI.TransitionToState(AI.ChasingState);
            }
        }
    }
}
