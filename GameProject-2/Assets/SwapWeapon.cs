using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapWeapon : MonoBehaviour
{
    [SerializeField]private List<GameObject> weapons;
    
    //Upon called compares a weapon ID in the list weapons with the weaponName parameter. If they match it activates
    //The weapons mesh in the prefab.
    public void EquipWeapon(string weaponName)
    {
        foreach (var weapon in weapons)
        {
            if (weaponName == "null")
            {
                break;
            }
            
            weapon.SetActive(weaponName == weapon.GetComponent<WeaponID>().weaponID);
        }
    }
}
