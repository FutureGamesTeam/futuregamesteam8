using UnityEngine;
using UnityEngine.EventSystems;

//Makes it so this script runs first
[DefaultExecutionOrder(-1)]
public class InputManager : MonoBehaviour
{
    #region Variables
    
    private float _zoomSensitivity = 0.002f; //dont trust Rider, it is being used in android code
    private Camera _mainCamera;
    private bool _newTouch; //dont trust Rider, it is being used in android code

    private ParticleSystem _clickEffect;

    #endregion
    

    private void Awake()
    {
        _clickEffect = GetComponentInChildren<ParticleSystem>();
        _mainCamera = Camera.main;
        _newTouch = true;
    } //assigns the camera to the variable.

    private void Update()
    {
#if UNITY_STANDALONE_WIN 
        
        EventHandler.Current.Zoom(Input.mouseScrollDelta.y);
        
        if (Input.GetMouseButtonDown(1))
        {
            EventHandler.Current.DragClick(Input.mousePosition);
        } //if right mouse button is initially clicked
        
        if (Input.GetMouseButton(1))
        {
            EventHandler.Current.Drag(Input.mousePosition);
        } //while right mouse button is being held down

#endif //if the game is running on windows it runs the zoom event all the time that is used in CameraController

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        } //closes the game down if you press the escape button
        
        if (Input.GetMouseButtonDown(0))
        {
            StartClick();
        } //if left mouse button is initially clicked (OR a touch tap) got pressed down

        
#if UNITY_ANDROID

        if (Input.touchCount > 1)
        {
            if (_newTouch)
            {
                EventHandler.Current.DragClick(Input.mousePosition);
                _newTouch = false;
            } //this runs once when 2 or more touch inputs are being held down to get the initial mouse position which is the average of all inputs
            
            var touchZero = Input.GetTouch(0);
            var touchOne = Input.GetTouch(1);
            //gets touch 1 and 2 to use for zoom

            var touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            var touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
            //sets the original touch position by subtracting the delta on each touch input

            var prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            var currentMagnitude = (touchZero.position - touchOne.position).magnitude;
            //sets the initial and current magnitude

            var difference = currentMagnitude - prevMagnitude;
            //subtracts the initial magnitude from the current to see if we are pinching or stretching

            EventHandler.Current.Zoom(difference* _zoomSensitivity);
            EventHandler.Current.Drag(Input.mousePosition);
            //runs the events for dragging and zooming
            //dragging only uses mousePosition because its the average between all touch inputs and works well with panning the camera
        } //if there is more than 1 touch input

        if (Input.touchCount < 1)
        {
            _newTouch = true;
        } //when there is less than 1 touch inputs set _newTouch to true so that it can set a new origin point again on the next double touch 

#endif //if the game is running on android
    } //end of update 
    
    private void StartClick()
    {
        if (EventSystem.current.IsPointerOverGameObject()) return;
        
        var ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (!Physics.Raycast(ray, out hit)) return; //if we dont hit anything, return
        switch (hit.collider.gameObject.layer) //switch that checks the layer of the object we hit
        { 
            case 5:
                break;
            case 7: //Floor movement
                EventHandler.Current.MoveClick(hit.point); //event for moving if a character is selected
                _clickEffect.gameObject.transform.position = new Vector3(hit.point.x, hit.point.y + 0.1f, hit.point.z);
                _clickEffect.Play();
                break;
            case 8: //Player Unit select
                hit.collider.gameObject.GetComponent<PlayerFSM>().Selected(true);
                break;
            case 9: //Enemy 
                EventHandler.Current.AttackGO(hit.collider.gameObject);
                break;
            default: //failsafe in case there is no layer (or not in this switch list) on the object
                break;
        }
    }
}
