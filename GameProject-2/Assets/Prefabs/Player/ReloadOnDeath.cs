using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReloadOnDeath : MonoBehaviour
{
    [SerializeField]private PlayerFSM PC;


    private void Start()
    {
        PC = GetComponent<PlayerFSM>();
    }

    void Update()
    {
        if (PC.healthPoints < 1)
        {
            SceneManager.LoadScene(0);
        }
    }
}
