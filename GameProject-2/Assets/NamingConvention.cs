using UnityEngine;

//==========GIT WORKFLOW==========//
//
// Work on your own branch only!
// Branch merging requires approval from at least one programmer (Not yourself)
//
//================================//

public class NamingConvention : MonoBehaviour
{
    public static NamingConvention Current;

    private int _nameOne;
    public int nameTwo;
    [SerializeField] private int nameThree;

    private void Start()
    {
        NameOne();
    }

    private void NameOne()
    {
        _nameOne = 1;
    }

    public void NameTwo()
    {
        nameTwo = _nameOne;
    }
}
