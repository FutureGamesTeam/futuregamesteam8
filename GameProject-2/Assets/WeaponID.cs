using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//ID for a specific weapon. The ID is compared in the SwapWeapon script.
public class WeaponID : MonoBehaviour
{
    public string weaponID;
}
